# README #


### What is this repository for? ###

This project is used to test an open source [REST API](https://api.openweathermap.org/data/2.5/weather) using `pytest`.



## Installation

#### Prerequisites
- [python 3](https://www.python.org/downloads/)
- [pip](https://pip.pypa.io/en/stable/installing/)
- use `pip` and `requirements.txt` file to easy install all packages with one command using [my-requirements.txt](https://bitbucket.org/alexpatrascu1/cartatest/src/master/resources/my-requirements.txt) file:
```jshelllanguage
$ pip install -r resources/my-requirements.txt
```

#### Summary of set up

##### Configuration

The project folder stucture is easy to use and to follow:

- `test` directory contains two files: `conftest.py` which has the hooks and fixtures needed for initialization and `test_weather_api.py`, file that contains the tests for out Weather API
- configuration settings in `pytest.ini`
- `resources` directory contains data files; in case of multiple environment with different configurations, we can use the `--env` CLI option to choose our desired configuration (default `test.ini`)
- `report.html` is generated at the end of the test run

##### How to run the tests
- to run the full suit of tests tests:
```
$ pytest -v -s -ra --html=report.html
```
- to run a specific test `-k` using a specific environment `--env`
```
$ pytest --env=test -k "zip" -v -s -ra --html=report.html
```
- to run a certain category of tests, we can use markers `-m`
```
$ pytest --env=test -m poc  -v -s -ra --html=report.html
```

- at the end of the test run a report file `report.html`  will be generated under root directory 
  


##### Who do I talk to? ###

* [Alex Patrascu](mailto:patrascual@gmail.com)
