import pytest
import os
import logging
from configparser import ConfigParser


LOGGER = logging.getLogger(__name__)

def pytest_addoption(parser):
    """
    Pytest hook that defines list of CLI arguments with descriptions
    and default values
    :param parser: pytest specific argument
    :return: void 
    """ 
    parser.addoption("--env", action="store", default="test",     
                help="select in what environment to run the tests")


@pytest.fixture(scope="session")
def get_config(request):
    """
    Function that reads and return ConfigParser object that access 
    the config file
    :rtype: ConfigParser
    """
    _environment = request.config.getoption("--env")
    _config_parser = ConfigParser()
    _ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    _file_path = os.path.join(_ROOT_DIR, 
                    '../resources/{}.ini'.format(_environment))
    LOGGER.info(f"Reading the config file {_file_path}")
    try:
        _config_parser.read(_file_path)
    except OSError as err:
        LOGGER.error("ConfigParser OS error: {0}".format(err))
    return _config_parser


@pytest.fixture(scope="session")
def get_api_details(get_config):
    """
    Fixture that returns API URL and key
    :param get_config: fixture that returns ConfigParser object 
    that access the config file
    :type: ConfigParser

    :return: Returns API connection URL and API Key
    :rtype: List[str]
    """
    LOGGER.info("Getting the API details from config file")
    try:
        return get_config['API']['url'], get_config['API']['appid']
    except KeyError as err:
        LOGGER.error(f"Could not get the configuration due to KeyError: {err}")
        
