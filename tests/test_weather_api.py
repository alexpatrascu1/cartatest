import pytest
import logging
import requests

LOGGER = logging.getLogger(__name__)

def get_data(api_url, payload):
    """
    Query the URL by the city name
    Verify name attribute corresponds to the entered city name
    :param api_url: url for our rest API
    :type: str
    :param payload: list of parameters used for the GET call
    :type: dict
    
    :return: return the API response body
    :rtype: dict
    """
    
    LOGGER.info("Get the API response ")
    try:
        _r = requests.get(api_url, params=payload)
        if (_r.status_code == 200): 
            LOGGER.info(f"GET call was successful, response code: {_r.status_code}")
        else:
            LOGGER.error(f"GET call failed with response code: {_r.status_code}")
    except Exception as err:
        LOGGER.error(f"Could not send the GET request due to: {err}")
    return _r.json()


@pytest.mark.poc
@pytest.mark.parametrize("city_name,",["New York", "Hoboken"])
def test_verify_city_name(get_api_details, city_name):
    """
    Query the URL by the city name
    Verify name attribute corresponds to the entered city name
    :param get_api_details: list that returns API URL and API key
    :type: List[str]
    :param city_name: list of city names
    :type: List[str]  
    """
    LOGGER.info("Verify name attribute in API respose matches city name")
    _api_url = get_api_details[0]
    _api_key = get_api_details[1] 
    _payload = {'appid': _api_key, 'q': city_name}
    
    try:
        _data = get_data(_api_url, _payload)
        assert _data["name"] == city_name
        LOGGER.info(f"Name attribute corresponds to the entered city name {city_name}")
    except Exception as err:
        LOGGER.error(f"Decoding failed due to: {err}")
    

@pytest.mark.poc
@pytest.mark.parametrize("lat, lon",[
    (40.7445, -74.0329), 
    (40.7599, -74.4179)
])
def test_verify_coordinates(get_api_details, lat, lon):
    """
    Query the URL by latitude and longitude
    Verify `coord.lat` and `coord.lon` attributes correspond to the values entered
    :param get_api_details: list that returns API URL and API key
    :type: List[str]
    :param lat: latitude coordinate of a city
    :type: float
    :param lat: latitude coordinate of a city
    :type: float 
    """
    LOGGER.info("Verify lat and lon attributes correspond to the values entered")
    _api_url = get_api_details[0]
    _api_key = get_api_details[1]
    _payload = {'appid': _api_key, 'lat': lat, 'lon': lon}

    try:
        _data = get_data(_api_url, _payload)
        assert _data["coord"]["lon"] == lon
        assert _data["coord"]["lat"] == lat
        LOGGER.info(f"Coordinates correspond to the values provided {lon} {lat}")
    except Exception as err:
        LOGGER.error(f"Decoding failed due to: {err}")
    

@pytest.mark.poc
@pytest.mark.parametrize("zip_code, city_name",[
    ("07030", "Hoboken"),
    ("07940", "Madison")
    ])
def test_verify_zip_code(get_api_details, zip_code, city_name):
    """
    Query the URL by ZIP code
    Verify name attribute corresponds to the city that contains the ZIP code
    :param zip: city zipcode
    :type: int
    :param city_name: expected city name for zipcode provided
    :type: str 
    """
    LOGGER.info("Verify name attribute correspond to the zipcode")
    _api_url = get_api_details[0]
    _api_key = get_api_details[1]
    _payload = {'appid': _api_key, 'zip': zip_code}

    try:
        _data = get_data(_api_url, _payload)
        assert _data["name"] == city_name
        LOGGER.info(f"City name {city_name} from API response corresponds to zipcode provided {zip_code}")
    except Exception as err:
        LOGGER.error(f"Decoding failed due to: {err}")

